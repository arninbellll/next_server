import React from 'react'
import Layout from '../components/Layout';


const about = () => {
  return (
      <Layout title ="เกี่ยวกับเรา">
      <div className="container col-md-5">
        <h3>สวัสดีค่ะ</h3>
        <p className="title text-justify mt-4 mb-4">
          เราคือร้านอาหารที่เน้นอาหารอร่อย
          มีความหลากหลายทั้งของคาวเเละของหวาน
          เเต่เมื่อรับประทานเเล้วอย่าลืมดูเเลสุขภาพด้วยนะคะ
        </p>
        <h4 className="text-success">จาก เฮลตี้ คาเฟ่</h4>
      </div>
      </Layout>

  )

}
export default about;
